
import scrapy

from ..items import SoccerItem


class SoccerPlayersSpider(scrapy.Spider):
    name = "soccer_players"
    start_urls = [
        'https://www.flamengo.com.br/elencos/elenco-profissional',
    ]

    def parse(self, response):
        for soccer in response.css('.elenco-atleta'):
            soccer_item = SoccerItem()
            soccer_item['popular_name'] = soccer.css('figcaption .mb-0::text').get()
            soccer_item['main_image'] = soccer.css('img::attr(src)').get()
            soccer_item['link'] = soccer.css('a::attr(href)').get()
            yield scrapy.Request( soccer_item['link'], callback=self.parseSoccerDetails, meta={'SoccerItem': soccer_item})

    def parseSoccerDetails(self, response):
            soccer_item = response.meta['SoccerItem']
            soccer_item['number'] = response.css('.shirt-number::text').get(),
            soccer_item['full_name']= response.xpath('/html/body/section/section[2]/div/div/div/div[2]/div/div/p/span/text()').get()
            soccer_item['position']= response.xpath('/html/body/section/section[2]/div/div/div/div[2]/div/div/ul[1]/li[1]/text()').get()
            soccer_item['image_gallery']=  response.css('.col-6 a::attr(href)').getall()
            return soccer_item
