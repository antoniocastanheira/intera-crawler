import scrapy


class SoccerItem(scrapy.Item):
    popular_name = scrapy.Field()
    full_name = scrapy.Field()
    main_image = scrapy.Field()
    link = scrapy.Field()
    number = scrapy.Field()
    position = scrapy.Field()
    image_gallery = scrapy.Field()
