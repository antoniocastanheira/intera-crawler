# Intera Crawler

## Requirements
- Python 3.8
- pip
- virtualenv*

 `*` recommended, but not necessary


## Check if python is installed
`python -v # Should have python3.8`

## Install Scrapy framework
`pip install Scrapy`

## Run spider at Flamengo's site
`scrapy crawl soccer_players --output "temp/%(name)s-%(time)s.json" --output-format json`

## Show the results at console
`cat temp/$(ls temp -q | tail -1)`
